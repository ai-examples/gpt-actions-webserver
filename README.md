# GPT Actions Webserver
An example template to construct a GPT Actions webserver

## Overview
| Component       | Description                                                                                                            |
|-----------------|------------------------------------------------------------------------------------------------------------------------|
| Web Application | The web application, written in Python, will serve example data for the ChatGPT Action to consume. It will also serve different files needed by ChatGPT. |
| Nginx           | Nginx will be used to configure Let's Encrypt and redirect all traffic to use SSL                                       |
| Docker Compose  | Docker compose will be used to configure the internal network and link the web application with Nginx using a single `docker compose` command |
| OpenAPI.json    | This file will be used as the OpenAPI Schema that the ChatGPT Action needs to interpret the commands and return data to/from the server |
| SSL Certificate | A certificate will be needed into order to set up SSL with the webserver (as it is a requirement for ChatGPT Actions). It will be ignored in the Git history since it is a private file |

## Requirements
Run `pip install -r requirements.txt` from the root of this repository

## Configuration
### Getting a domain name
There are a myriad of ways to get a domain name. For this example project that is out of scope. One simple way of setting up a domain that points to your home server would be through the use of a Dynamic DNS. 

No-ip.org is a good and free service to route a domain name to your home IP address. You will need to make sure to create an A record instead of a URL redirect. 

### SSL certificate using Let's Encrypt
Before running the Docker Compose, you need to obtain an SSL certificate. Certbot is commonly used for this. Here's a manual command to get you started, assuming you're not already using port 80:
```
sudo certbot certonly --standalone --agree-tos -v --preferred-challenges http -d your_domain.com
```
Create a folder called `certbot` and place the output files into this repository. Here's a list of file that should have been generated:
| File           | Description                                                   |
|----------------|---------------------------------------------------------------|
| `privkey.pem`  | The private key for your certificate.                         |
| `fullchain.pem`| The certificate file used in most server software.             |
| `chain.pem`    | Used for OCSP stapling in Nginx >=1.3.7.                      |
| `cert.pem`     | Will break many server configurations, and should not be used without reading further documentation (see link below). |

**Note: Do not share these files with anyone. They are ignored in the `.gitignore` to ensure they do not get committed to your repository.**

### Nginx
After obtaining your SSL certificate, we'll need configure Nginx to use SSL by routing traffic through HTTPS.

Create a folder called `nginx` and create a new file (in that folder) called `default.conf`. Using this template, replace `your_domain.com` with the domain name that was used when you requested your SSL certificate using `certbot`:
```
server {
    listen 80;
    server_name your_domain.com;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    server_name your_domain.com;

    ssl_certificate /etc/ssl/certs/fullchain.pem;
    ssl_certificate_key /etc/ssl/private/privkey.pem;

    location / {
        proxy_pass http://web:8000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
```

### GPT Action
Your new GPT Action requires an OpenAPI schema to know where and what it's talking to. Paste the contents of `openapi.json` into the "Actions" section of your custom GPT's settings. Remember to replace `your_domain.com` with the domain name that was used when you requested your SSL certificate using `certbot`.

## Running the application
After completing the steps in the Configuration section (above) you can now run the server using the following command:
```
docker compose up
```
Once you've confirmed that you can hit your web server from the outside world (e.g. a smartphone with WiFi turned off will do) go back to your custom GPT actions settings and hit the "Test" button under the available actions.

At this point you should be up and running. Try asking questions like `what is the status of my server?` or `generate a random number for me` and observe your server getting a request from OpenAI's servers.

## Raspberry Pi
For a demonstration of GPT Actions controlling hardware a simple LED example has been set up with a Raspberry Pi

### How to build and run
On your Raspberry Pi make sure you have Docker installed then run the following command
```
BUILD_TYPE=rpi docker compose up --build
```