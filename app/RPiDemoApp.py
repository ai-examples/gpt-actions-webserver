import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import RPi.GPIO as GPIO
from DemoApp import DemoApp
import logging

# Configure logger
logging.basicConfig(level=logging.INFO)

class RPiDemoApp(DemoApp):
    LED_PIN = 26

    def __init__(self):
        super().__init__()

        # GPIO setup for Raspberry Pi
        GPIO.setmode(GPIO.BOARD)  # Physical pin number
        GPIO.setup(self.LED_PIN, GPIO.OUT)
        self.led_state = GPIO.LOW

        logging.info("GPIO has been configured")

    def setup_routes(self):
        """
        Extends the routes for the FastAPI application to include the new toggle_led route.
        """
        super().setup_routes()  # Call the setup_routes from the parent class
        self.app.get("/toggle_led")(self.toggle_led)  # Append the new toggle_led route

    def toggle_led(self):
        """
        This method toggles the state of the LED on the Raspberry Pi.
        """
        self.led_state = GPIO.HIGH if self.led_state == GPIO.LOW else GPIO.LOW
        GPIO.output(self.LED_PIN, self.led_state)
        return {"led_state": "HIGH" if self.led_state == GPIO.HIGH else "LOW"}
    
app = RPiDemoApp().app