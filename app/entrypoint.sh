#!/bin/bash

# Conditional command based on the BUILD_TYPE environment variable
if [ "$BUILD_TYPE" = "rpi" ]; then
    uvicorn RPiDemoApp:app --host 0.0.0.0 --port 8000
else
    uvicorn DemoApp:app --host 0.0.0.0 --port 8000
fi
