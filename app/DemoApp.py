import random
from fastapi import FastAPI

class DemoApp:
    def __init__(self):
        """
        Initialize the application and set up routes within the class.
        """
        self.app = FastAPI()
        self.setup_routes()

    def setup_routes(self):
        """
        Sets up the routes for the FastAPI application.
        """
        self.app.get("/")(self.root)
        self.app.get("/random_number")(self.random_number)

    async def root(self):
        """
        A method that handles the root endpoint and returns a dictionary with the status "OK".
        """
        return {"status": "OK"}

    async def random_number(self):
        """
        This method returns a random number between 1 and 100.
        """
        random_number = random.randint(1, 100)
        return {"number": random_number}

app = DemoApp().app