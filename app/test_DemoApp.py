import pytest
from fastapi.testclient import TestClient
from DemoApp import DemoApp

class TestDemoApp:
    @pytest.fixture
    def test_app(self):
        """
        Fixture for testing the app.
        """
        return TestClient(DemoApp().app)

    def test_root(self, test_app):
        """
        This function tests the root endpoint of the given test application.
        """
        response = test_app.get("/")
        assert response.status_code == 200
        assert response.json() == {"status": "OK"}

    def test_random_number(self, test_app):
        """
        Function to test the 'random_number' endpoint using the provided test_app.
        """
        response = test_app.get("/random_number")
        assert response.status_code == 200
        assert "number" in response.json()
        assert 1 <= response.json()["number"] <= 100