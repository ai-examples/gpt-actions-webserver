# Use the official Python 3.12 image as the base image
FROM python:3.12

# Argument to differentiate between standard and RPi builds
ARG BUILD_TYPE=standard
ENV BUILD_TYPE=${BUILD_TYPE}

# Set the working directory inside the container
WORKDIR /app

# Copy the base requirements.txt file from the local directory to the container
COPY app/requirements.txt .

# Optionally copy the requirements-rpi.txt if building for RPi
COPY app/requirements-rpi.txt requirements-rpi.txt

# Install the Python dependencies from requirements.txt
# Additionally install dependencies from requirements-rpi.txt if BUILD_TYPE is rpi
RUN pip install --no-cache-dir -r requirements.txt && \
    if [ "$BUILD_TYPE" = "rpi" ]; then pip install --no-cache-dir -r requirements-rpi.txt; fi

# Copy all the files from the local directory to the container
COPY app .

# Copy entrypoint script
COPY app/entrypoint.sh /app/entrypoint.sh

# Ensure the script is executable
RUN chmod +x /app/entrypoint.sh

# Use the entrypoint script to start the container
CMD ["/app/entrypoint.sh"]